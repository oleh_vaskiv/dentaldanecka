<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160826102055 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE employees DROP FOREIGN KEY FK_1GG399504E7AF8C');
        $this->addSql('DROP TABLE employees_categories');
        $this->addSql('ALTER TABLE advices RENAME INDEX idx_1dd399504e7af8f TO IDX_47DE8B044E7AF8F');
        $this->addSql('DROP INDEX FK_1GG399504E7AF8C ON employees');
        $this->addSql('ALTER TABLE employees ADD show_main TINYINT(1) DEFAULT NULL, ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME DEFAULT NULL, ADD published_at DATETIME DEFAULT NULL, DROP category_id, CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE name title VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE employees RENAME INDEX fk_1gg399504e7af8d TO IDX_387341A34E7AF8F');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE employees_categories (id INT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Advices RENAME INDEX idx_47de8b044e7af8f TO IDX_1DD399504E7AF8F');
        $this->addSql('ALTER TABLE Employees ADD category_id INT DEFAULT NULL, DROP show_main, DROP created_at, DROP updated_at, DROP published_at, CHANGE id id INT NOT NULL, CHANGE title name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE Employees ADD CONSTRAINT FK_1GG399504E7AF8C FOREIGN KEY (category_id) REFERENCES employees_categories (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX FK_1GG399504E7AF8C ON Employees (category_id)');
        $this->addSql('ALTER TABLE Employees RENAME INDEX idx_387341a34e7af8f TO FK_1GG399504E7AF8D');
    }
}
