<?php

namespace Ls\EmployeesBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Employees
 * @ORM\Table(name="employees")
 * @ORM\Entity
 */
class Employees
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=11)
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\Column(type="integer", length=11)
     * @var string
     */
    private $category_id;
    

    /**
     * @ORM\Column(type="boolean", length=1)
     * @var string
     */
    private $in_slider;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $post;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $old_slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seo_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_description;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $published_at;

    protected $file;

    protected $listWidth = 150;
    protected $listHeight = 200;
    protected $detailWidth = 300;
    protected $detailHeight = 400;
    protected $sliderWidth = 600;
    protected $sliderHeight = 400;

    protected $sliderTWidth = 340; //tablet
    protected $sliderTHeight = 400;

    protected $sliderMWidth = 300; //mobile
    protected $sliderMHeight = 245;

    protected $pageWidth = 300;
    protected $pageHeight = 200;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->seo_generate = true;
        $this->in_slider = false;
        $this->created_at = new \DateTime();
        $this->published_at = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement()
    {
        return $this->arrangement;
    }

    /**
     * Set arrangement
     *
     * @return integer
     */
    public function setArrangement($arrangement)
    {
        $this->arrangement = $arrangement;

        return $this;
    }


    /**
     * Set name
     *
     * @param string $name
     * @return Employees
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set post
     *
     * @param string $name
     * @return Employees
     */
    public function setPost($post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return string
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Employees
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set old_slug
     *
     * @param string $old_slug
     * @return Employees
     */
    public function setOldSlug($old_slug)
    {
        $this->old_slug = $old_slug;

        return $this;
    }

    /**
     * Get old_slug
     *
     * @return string
     */
    public function getOldSlug()
    {
        return $this->old_slug;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Employees
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return Employees
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set category_id
     *
     * @param boolean $category_id
     * @return Employees
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;

        return $this;
    }

    /**
     * Get category_id
     *
     * @return boolean
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }


    /**
     * Set in_slider
     *
     * @param boolean $in_slider
     * @return Employees
     */
    public function setInSlider($in_slider)
    {
        $this->in_slider = $in_slider;

        return $this;
    }

    /**
     * Get in_slider
     *
     * @return boolean
     */
    public function getInSlider()
    {
        return $this->in_slider;
    }


    /**
     * Set seo_generate
     *
     * @param boolean $seo_generate
     * @return Employees
     */
    public function setSeoGenerate($seo_generate)
    {
        $this->seo_generate = $seo_generate;

        return $this;
    }

    /**
     * Get seo_generate
     *
     * @return boolean
     */
    public function getSeoGenerate()
    {
        return $this->seo_generate;
    }

    /**
     * Set seo_title
     *
     * @param string $seo_title
     * @return Employees
     */
    public function setSeoTitle($seo_title)
    {
        $this->seo_title = $seo_title;

        return $this;
    }

    /**
     * Get seo_title
     *
     * @return string
     */
    public function getSeoTitle()
    {
        return $this->seo_title;
    }

    /**
     * Set seo_keywords
     *
     * @param string $seo_keywords
     * @return Employees
     */
    public function setSeoKeywords($seo_keywords)
    {
        $this->seo_keywords = $seo_keywords;

        return $this;
    }

    /**
     * Get seo_keywords
     *
     * @return string
     */
    public function getSeoKeywords()
    {
        return $this->seo_keywords;
    }

    /**
     * Set seo_description
     *
     * @param string $seo_description
     * @return Employees
     */
    public function setSeoDescription($seo_description)
    {
        $this->seo_description = $seo_description;

        return $this;
    }

    /**
     * Get seo_description
     *
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seo_description;
    }


    public function __toString()
    {
        if (is_null($this->getName())) {
            return 'NULL';
        }

        return $this->getName();
    }

    public function getThumbSize($type)
    {
        $size = array();
        switch ($type) {
            case 'list':
                $size['width'] = $this->listWidth;
                $size['height'] = $this->listHeight;
                break;
            case 'detail':
                $size['width'] = $this->detailWidth;
                $size['height'] = $this->detailHeight;
                break;
            case 'slider':
                $size['width'] = $this->sliderWidth;
                $size['height'] = $this->sliderHeight;
                break;

            case 'slider_tablet':
                $size['width'] = $this->sliderTWidth;
                $size['height'] = $this->sliderTHeight;
                break;

            case 'slider_mobile':
                $size['width'] = $this->sliderMWidth;
                $size['height'] = $this->sliderMHeight;
                break;

            case 'page':
                $size['width'] = $this->pageWidth;
                $size['height'] = $this->pageHeight;
                break;
        }

        return $size;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return News
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     * @return News
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set published_at
     *
     * @param \DateTime $published_at
     * @return News
     */
    public function setPublishedAt($published_at)
    {
        $this->published_at = $published_at;

        return $this;
    }

    /**
     * Get published_at
     *
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->published_at;
    }

    public function getThumbWebPath($type)
    {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'list':
                    $sThumbName = Tools::thumbName($this->photo, '_l');
                    break;
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
                case 'slider':
                    $sThumbName = Tools::thumbName($this->photo, '_s');
                    break;
                case 'slider_tablet':
                    $sThumbName = Tools::thumbName($this->photo, '_st');
                    break;
                case 'slider_mobile':
                    $sThumbName = Tools::thumbName($this->photo, '_sm');
                    break;
                case 'page':
                    $sThumbName = Tools::thumbName($this->photo, '_p');
                    break;
            }

            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type)
    {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'list':
                    $sThumbName = Tools::thumbName($this->photo, '_l');
                    break;
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
                case 'slider':
                    $sThumbName = Tools::thumbName($this->photo, '_s');
                    break;
                case 'slider_tablet':
                    $sThumbName = Tools::thumbName($this->photo, '_st');
                    break;
                case 'slider_mobile':
                    $sThumbName = Tools::thumbName($this->photo, '_sm');
                    break;
                case 'page':
                    $sThumbName = Tools::thumbName($this->photo, '_p');
                    break;
            }

            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize()
    {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width'  => $temp[0],
            'height' => $temp[1],
        );

        return $size;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function deletePhoto()
    {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_l = Tools::thumbName($filename, '_l');
            $filename_d = Tools::thumbName($filename, '_d');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_l)) {
                @unlink($filename_l);
            }
            if (file_exists($filename_d)) {
                @unlink($filename_d);
            }
        }
    }

    public function getPhotoAbsolutePath()
    {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getPhotoWebPath()
    {
        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/employees';
    }

    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $this->createThumbs();

        unset($this->file);
    }

    public function createThumbs()
    {
        if (null !== $this->getPhotoAbsolutePath()) {
            $sFileName = $this->getPhoto();
            $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

            //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameL = Tools::thumbName($sSourceName, '_l');
            $aThumbSizeL = $this->getThumbSize('list');
            $thumb->adaptiveResize($aThumbSizeL['width'] + 2, $aThumbSizeL['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeL['width'], $aThumbSizeL['height']);
            $thumb->save($sThumbNameL);

            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameD = Tools::thumbName($sSourceName, '_d');
            $aThumbSizeD = $this->getThumbSize('detail');
            $thumb->adaptiveResize($aThumbSizeD['width'] + 2, $aThumbSizeD['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeD['width'], $aThumbSizeD['height']);
            $thumb->save($sThumbNameD);

            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameS = Tools::thumbName($sSourceName, '_s');
            $aThumbSizeS = $this->getThumbSize('slider');
            $thumb->adaptiveResize($aThumbSizeS['width'] + 2, $aThumbSizeS['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeS['width'], $aThumbSizeS['height']);
            $thumb->save($sThumbNameS);

            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameST = Tools::thumbName($sSourceName, '_st');
            $aThumbSizeST = $this->getThumbSize('slider_tablet');
            $thumb->adaptiveResize($aThumbSizeST['width'] + 2, $aThumbSizeST['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeST['width'], $aThumbSizeST['height']);
            $thumb->save($sThumbNameST);

            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameSM = Tools::thumbName($sSourceName, '_sm');
            $aThumbSizeSM = $this->getThumbSize('slider_mobile');
            $thumb->adaptiveResize($aThumbSizeSM['width'] + 2, $aThumbSizeSM['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeSM['width'], $aThumbSizeSM['height']);
            $thumb->save($sThumbNameSM);

            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameP = Tools::thumbName($sSourceName, '_p');
            $aThumbSizeP = $this->getThumbSize('page');
            $thumb->adaptiveResize($aThumbSizeP['width'] + 2, $aThumbSizeP['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeP['width'], $aThumbSizeP['height']);
            $thumb->save($sThumbNameP);

        }
    }

    public function Thumb($x, $y, $x2, $y2, $type)
    {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = \PhpThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 2, $aThumbSize['height'] + 2);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }

}