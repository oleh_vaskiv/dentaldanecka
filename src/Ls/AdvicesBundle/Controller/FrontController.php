<?php

namespace Ls\AdvicesBundle\Controller;

use Doctrine\DBAL\Types\Type;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Advices controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all Advices entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $limit = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('limit_advices')->getValue();
        $allow = 1;

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsAdvicesBundle:Advices', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        $count = $em->createQueryBuilder()
             ->select('COUNT(u)')
             ->from('LsEmployeesBundle:Employees', 'u')
             ->getQuery()
             ->getSingleScalarResult();

        $random_employeer = $em->createQueryBuilder()
            ->select('e')
            ->from('LsEmployeesBundle:Employees', 'e')
            ->setFirstResult(rand(0, $count - 1))
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();



        foreach ($entities as $entity) {
            if ($entity->getPhotoWebPath()) {
                $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 170, '...'));
            } else {
                $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 255, '...'));
            }
        }

        if (count($entities) < $limit) {
            $allow = 0;
        }

        return $this->render('LsAdvicesBundle:Front:index.html.twig', array(
            'entities' => $entities,
            'allow' => $allow,
            'employeer' => $random_employeer,
        ));
    }

    /**
     * Finds and displays a Advices entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $today = new \DateTime();
        $qb = $em->createQueryBuilder();
        $entities = $qb->select('s')
            ->from('LsAdvicesBundle:Advices', 's')
            ->where($qb->expr()->isNotNull('s.published_at'))
            ->andWhere('s.published_at <= :today')
            ->andWhere('s.slug <> :slug')
            ->orderBy('s.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setParameter('slug', $slug)
            ->setMaxResults(4)
            ->getQuery()
            ->getResult();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsAdvicesBundle:Advices', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Advices entity.');
        }

        return $this->render('LsAdvicesBundle:Front:show.html.twig', array(
            'entity' => $entity,
            'entities' => $entities,
        ));
    }

    public function ajaxMoreAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $page = $request->request->get('page');
        $limit = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('limit_advices')->getValue();
        $start = $page * $limit;
        $allow = 1;

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsAdvicesBundle:Advices', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        foreach ($entities as $entity) {
            if ($entity->getPhotoWebPath()) {
                $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 170, '...'));
            } else {
                $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 255, '...'));
            }
        }

        if (count($entities) < $limit) {
            $allow = 0;
        }

        $response = array(
            'allow' => $allow,
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsFormBundle:Advices:list.html.twig', array(
                'entities' => $entities,
            ))->getContent())
        );

        return new JsonResponse($response);
    }
}
