<?php

namespace Ls\OfferBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CoreBundle\Utils\Tools;
use Ls\OfferBundle\Entity\Offer;

class OfferUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'postPersist',
            'preUpdate',
            'postUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        if ($entity instanceof Offer) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
            if ($entity->getSeoGenerate()) {
                // description
                // usunięcie ewentualnego tagu style wraz z jego zawartością
                $description = preg_replace('/<style.+\/style>/su', '', $entity->getContent());
                $description = strip_tags($description);
                $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                // usunięcie nowych linii i podwójnych białych znaków
                $description = preg_replace('/\s\s+/u', ' ', $description);
                $description = trim($description);
                // skrócenie do około 160 znaków rekomendowanych przez Google
                $description = Tools::truncateWord($description, 155, '...');

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoKeywords(implode(', ', $keywords));
                $entity->setSeoGenerate(false);
            }


            if ($entity->getParentId() != null)
            {
                $parent_id = $entity->getParentId();
                $update_last = $em->getRepository('Ls\OfferBundle\Entity\Offer')->find($parent_id);
                $update_last->setLast(false);

            }
            if (null === $entity->getArrangement()) {
                $parent_id = $entity->getParentId();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsOfferBundle:Offer', 'c')
                    ->where('c.parent_id = :parent_id')
                    ->setParameter('parent_id', $parent_id)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postPersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Offer) {
            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
                if (null !== $entity->getOldSlug()) {
                    $query = $em->createQueryBuilder()
                        ->select('c')
                        ->from('LsMenuBundle:MenuItem', 'c')
                        ->where('c.route LIKE :route')
                        ->andWhere('c.route_parameters LIKE :slug')
                        ->setParameter('route', 'ls_offer_show')
                        ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
                        ->getQuery();

                    $items = $query->getResult();

                    foreach ($items as $item) {
                        $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
                        $em->persist($item);
                    }
                    $em->flush();

                    $entity->setOldSlug($entity->getSlug());
                    $em->persist($entity);
                    $em->flush();
                } else {
                    $entity->setOldSlug($entity->getSlug());
                    $em->persist($entity);
                    $em->flush();
                }
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Offer) {
            $entity->setUpdatedAt(new \DateTime());
            if ($entity->getSeoGenerate()) {
                // description
                // usunięcie ewentualnego tagu style wraz z jego zawartością
                $description = preg_replace('/<style.+\/style>/su', '', $entity->getContent());
                $description = strip_tags($description);
                $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                // usunięcie nowych linii i podwójnych białych znaków
                $description = preg_replace('/\s\s+/u', ' ', $description);
                $description = trim($description);
                // skrócenie do około 160 znaków rekomendowanych przez Google
                $description = Tools::truncateWord($description, 155, '...');

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoKeywords(implode(', ', $keywords));
                $entity->setSeoGenerate(false);
            }

            if (null === $entity->getArrangement()) {
                $parent_id = $entity->getParentId();
                $id = $entity->getId();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsOfferBundle:Offer', 'c')
                    ->where('c.parent_id = :parent_id')
                    ->andWhere('c.id = :id')
                    ->setParameter('parent_id', $parent_id)
                    ->setParameter('id', $id)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Offer) {
            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsMenuBundle:MenuItem', 'c')
                    ->where('c.route LIKE :route')
                    ->andWhere('c.route_parameters LIKE :slug')
                    ->setParameter('route', 'ls_offer_show')
                    ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
                    ->getQuery();

                $items = $query->getResult();

                foreach ($items as $item) {
                    $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
                    $em->persist($item);
                }
                $em->flush();

                $entity->setOldSlug($entity->getSlug());
                $em->persist($entity);
                $em->flush();
            }
        }
    }



    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        if ($entity instanceof Offer) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();
                $parent_id = $entity->getParentId();
                
                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsOfferBundle:Offer', 'c')
                    ->where('c.arrangement > :arrangement')
                    ->andWhere('c.parent_id = :parent_id')
                    ->setParameter('arrangement', $arrangement)
                    ->setParameter('parent_id', $parent_id)
                    ->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['class'] = 'LsOfferBundle:Offer';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
        }
    }

}