<?php

namespace Ls\OfferBundle\Form;

use Ls\CoreBundle\Form\DataTransformer\DateTimeTransformer;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Ls\GalleryBundle\Form\HasGalleryType;

class OfferType extends HasGalleryType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('slug', null, array(
            'label' => 'Końcówka adresu URL'
        ));
        $builder->add('parent_id', HiddenType::class, array(
        ));
        $builder->add('content', null, array(
            'label' => 'Treść'
        ));
        $builder->add('seo_generate', null, array(
            'label' => 'Generuj automatycznie'
        ));
        $builder->add('seo_title', null, array(
            'label' => 'Tag "title"'
        ));
        $builder->add('seo_keywords', TextareaType::class, array(
            'label' => 'Meta "keywords"'
        ));
        $builder->add('seo_description', TextareaType::class, array(
            'label' => 'Meta "description"',
            'attr' => array(
                'rows' => 3
            )
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\OfferBundle\Entity\Offer',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_offer';
    }
}
