<?php

namespace Ls\EmployeesBundle\Controller;

use Ls\EmployeesBundle\Entity\Employees;
use Ls\EmployeesBundle\Entity\StaticContent;
use Ls\EmployeesBundle\Entity\EmployeesCategories;
use Ls\EmployeesBundle\Form\EmployeesType;
use Ls\EmployeesBundle\Form\StaticType;
use Ls\EmployeesBundle\Form\EmployeesCategoriesType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller {
    private $pager_limit_name = 'admin_employees_pager_limit';

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('LsEmployeesBundle:EmployeesCategories', 'e')
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.arrangement',
                'defaultSortDirection' => 'asc',
            )
        );
        
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_employees'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Zespół', $this->get('router')->generate('ls_admin_employees'));

        return $this->render('LsEmployeesBundle:Admin:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
        ));
    }

    public function newAction($id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new Employees();
        $size = $entity->getThumbSize('slider');
        $form = $this->createForm(EmployeesType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_employees_new', array('id' => $id)),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            if (null !== $entity->getFile()) {
                $sFileName = uniqid('employees-image-') . '.' . $entity->getFile()->guessExtension();
                $entity->setPhoto($sFileName);
                $entity->upload();
            }
            $em->persist($entity);
            $em->flush();

            $sitemap = $this->get('ls_core.sitemap');
            $sitemap->generate();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie pracownika zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_employees_edit', array('id' => $entity->getId(), 'category_id' => $id)));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_employees_category_list', array('id' => $id)));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_employees_new', array('id' => $id)));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }
        $category = $em->getRepository('LsEmployeesBundle:EmployeesCategories')->find($id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Zespół', $this->get('router')->generate('ls_admin_employees'));
        $breadcrumbs->addItem($category->__toString(), $this->get('router')->generate('ls_admin_employees_category_list', array('id' => $id)));

        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_employees_new', array('id' => $id)));

        return $this->render('LsEmployeesBundle:Admin:new.html.twig', array(
            'form' => $form->createView(),
            'size' => $size,
            'category_id' => $id,
            'category_name' => $category->__toString()
        ));
    }

    public function editAction($category_id, $id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsEmployeesBundle:Employees')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Employees entity.');
        }
        $size = $entity->getThumbSize('slider');

        $form = $this->createForm(EmployeesType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_employees_edit', array('id' => $entity->getId(), 'category_id' => $category_id)),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            if (null !== $entity->getFile()) {
                $entity->deletePhoto();

                $sFileName = uniqid('employees-image-') . '.' . $entity->getFile()->guessExtension();
                $entity->setPhoto($sFileName);
                $entity->upload();
            }
            $em->persist($entity);
            $em->flush();

            $sitemap = $this->get('ls_core.sitemap');
            $sitemap->generate();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja pracownika zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_employees_edit', array('id' => $entity->getId(), 'category_id' => $category_id)));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_employees_category_list', array('id' => $category_id)));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }
        $category = $em->getRepository('LsEmployeesBundle:EmployeesCategories')->find($category_id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Zespół', $this->get('router')->generate('ls_admin_employees'));
        $breadcrumbs->addItem($category->getCategoryName(), $this->get('router')->generate('ls_admin_employees_category_list', array('id' => $category_id)));

        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_employees_edit', array('id' => $entity->getId(), 'category_id' => $category_id)));

        return $this->render('LsEmployeesBundle:Admin:edit.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
            'size' => $size,
            'category_id' => $category_id,
            'category_name' => $category->getCategoryName()
        ));
    }

    public function deleteAction($category_id, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsEmployeesBundle:Employees')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Employees entity.');
        }

        $em->remove($entity);
        $em->flush();

        $sitemap = $this->get('ls_core.sitemap');
        $sitemap->generate();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie pracownika zakończone sukcesem.');

        return new Response('OK');
    }

    public function deleteCategoryAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsEmployeesBundle:EmployeesCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Employees entity.');
        }

        $em->remove($entity);
        $em->flush();

        $sitemap = $this->get('ls_core.sitemap');
        $sitemap->generate();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie kategorii zakończone sukcesem.');

        return new Response('OK');
    }

    public function kadrujAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $type = $request->get('type');

        $entity = $em->getRepository('LsEmployeesBundle:Employees')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Employees entity.');
        }

        if (null === $entity->getPhotoAbsolutePath()) {
            return $this->redirect($this->generateUrl('ls_admin_employees'));
        } else {
            $category_id = $entity->getCategoryId();
            $size = $entity->getThumbSize($type);
            $photo = $entity->getPhotoSize();
            $thumb_ratio = $size['width'] / $size['height'];
            $photo_ratio = $photo['width'] / $photo['height'];

            $thumb_conf = array();
            $thumb_conf['photo_width'] = $photo['width'];
            $thumb_conf['photo_height'] = $photo['height'];
            if ($thumb_ratio < $photo_ratio) {
                $thumb_conf['width'] = round($photo['height'] * $thumb_ratio);
                $thumb_conf['height'] = $photo['height'];
                $thumb_conf['x'] = ceil(($photo['width'] - $thumb_conf['width']) / 2);
                $thumb_conf['y'] = 0;
            } else {
                $thumb_conf['width'] = $photo['width'];
                $thumb_conf['height'] = round($photo['width'] / $thumb_ratio);
                $thumb_conf['x'] = 0;
                $thumb_conf['y'] = ceil(($photo['height'] - $thumb_conf['height']) / 2);
            }

            $preview = array();
            $preview['width'] = 150;
            $preview['height'] = round(150 / $thumb_ratio);

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Zespół', $this->get('router')->generate('ls_admin_employees'));
            $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_employees_edit', array('id' => $entity->getId(), 'category_id' => $category_id)));
            $breadcrumbs->addItem('Kadrowanie', $this->get('router')->generate('ls_admin_employees_crop', array('id' => $entity->getId(), 'type' => $type)));

            return $this->render('LsEmployeesBundle:Admin:kadruj.html.twig', array(
                'entity' => $entity,
                'preview' => $preview,
                'thumb_conf' => $thumb_conf,
                'size' => $size,
                'aspect' => $thumb_ratio,
                'type' => $type,
                'category_id' => $category_id,
            ));
        }
    }

    public function kadrujZapiszAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $type = $request->get('type');
        $x = $request->get('x');
        $y = $request->get('y');
        $x2 = $request->get('x2');
        $y2 = $request->get('y2');

        $entity = $em->getRepository('LsEmployeesBundle:Employees')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Employees entity.');
        }

        $entity->Thumb($x, $y, $x2, $y2, $type);
        $category_id = $entity->getCategoryID();

        $this->get('session')->getFlashBag()->add('success', 'Kadrowanie miniatury zakończone sukcesem.');

        return $this->redirect($this->generateUrl('ls_admin_employees_edit', array('id' => $entity->getId(), 'category_id' => $category_id)));
    }

    public function batchAction($category_id, Request $request) {
        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Zespół', $this->get('router')->generate('ls_admin_employees'));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_employees_batch', array('category_id' => $category_id)));

            return $this->render('LsEmployeesBundle:Admin:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
                'category_id' => $category_id,
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_employees_category_list', array('id' => $category_id )));
        }
    }

    public function batchExecuteAction($category_id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsEmployeesBundle:Employees', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }

                    $sitemap = $this->get('ls_core.sitemap');
                    $sitemap->generate();

                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_employees_category_list', array('id' => $category_id )));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_employees_category_list', array('id' => $category_id )));
        }
    }


    public function batchCategoryAction(Request $request) {
        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Zespół', $this->get('router')->generate('ls_admin_employees'));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_employees_category_batch'));

            return $this->render('LsEmployeesBundle:Admin:batch_category.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_employees'));
        }
    }

    public function batchExecuteCategoryAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsEmployeesBundle:EmployeesCategories', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }

                    $sitemap = $this->get('ls_core.sitemap');
                    $sitemap->generate();

                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_employees'));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_employees'));
        }
    }

    public function setLimitAction(Request $request) {
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }

    public function newCategoryAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new EmployeesCategories();

        $form = $this->createForm(EmployeesCategoriesType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_employees_category_new'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $sitemap = $this->get('ls_core.sitemap');
            $sitemap->generate();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie kategorii zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_employees_category_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_employees'));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_employees_category_new'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Zespół', $this->get('router')->generate('ls_admin_employees'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_employees_category_new'));

        return $this->render('LsEmployeesBundle:Admin:new_category.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editCategoryAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsEmployeesBundle:EmployeesCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Employees entity.');
        }

        $form = $this->createForm(EmployeesCategoriesType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_employees_category_edit', array('id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {

            $em->persist($entity);
            $em->flush();

            $sitemap = $this->get('ls_core.sitemap');
            $sitemap->generate();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja kategorii zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_employees_category_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_employees'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Zespół', $this->get('router')->generate('ls_admin_employees'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_employees_category_edit', array('id' => $entity->getId())));

        return $this->render('LsEmployeesBundle:Admin:edit_category.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
        ));
    }

    public function showEmployeesAction($id, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('LsEmployeesBundle:Employees', 'e')
            ->where("e.category_id = (:category_id)")
            ->setParameter('category_id', $id)
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.arrangement',
                'defaultSortDirection' => 'asc',
            )
        );
        
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_employees'));
        }
        $category = $em->getRepository('LsEmployeesBundle:EmployeesCategories')->find($id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Zespół', $this->get('router')->generate('ls_admin_employees'));
        $breadcrumbs->addItem($category->__toString(), $this->get('router')->generate('ls_admin_employees_category_list', array('id' => $category->getId())));

        return $this->render('LsEmployeesBundle:Admin:employees.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
            'category_id' => $category->getId(),
        ));
    }

    private function getMaxKolejnosc() {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsEmployeesBundle:EmployeesCategories', 'c')
            ->getQuery();

        $total = $query->getSingleScalarResult();
        return $total + 1;
    }

    private function getMaxEmployeesKolejnosc($category_id) {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsEmployeesBundle:Employees', 'c')
            ->where('c.category_id = :category_id')
            ->setParameter('category_id', $category_id)
            ->getQuery();

        $total = $query->getSingleScalarResult();
        return $total + 1;
    }

    public function moveCategoryDownAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsEmployeesBundle:EmployeesCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        $max = $this->getMaxKolejnosc();

        $old_kolejnosc = $entity->getArrangement();
        $new_kolejnosc = $old_kolejnosc + 1;
        if ($new_kolejnosc < $max) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsEmployeesBundle:EmployeesCategories', 'c')
                ->where('c.arrangement = :arrangement')
                ->setParameter('arrangement', $new_kolejnosc)
                ->getQuery();

            $old_entity_id = $query->getSingleScalarResult();
            $old_entity = $em->getRepository('LsEmployeesBundle:EmployeesCategories')->findOneById($old_entity_id);
            $old_entity->setArrangement(0);
            $em->persist($old_entity);
            $em->flush();
            $entity->setArrangement($new_kolejnosc);
            $em->persist($entity);
            $em->flush();
            $old_entity->setArrangement($old_kolejnosc);
            $em->persist($old_entity);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->add('success', 'Przeniesienie kategorii do dołu zakończone sukcesem.');

        return new Response('OK');
    }

    public function moveCategoryUpAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsEmployeesBundle:EmployeesCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        $old_kolejnosc = $entity->getArrangement();
        $new_kolejnosc = $old_kolejnosc - 1;
        if ($new_kolejnosc > 0) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsEmployeesBundle:EmployeesCategories', 'c')
                ->where('c.arrangement = :arrangement')
                ->setParameter('arrangement', $new_kolejnosc)
                ->getQuery();

            $old_entity_id = $query->getSingleScalarResult();
            $old_entity = $em->getRepository('LsEmployeesBundle:EmployeesCategories')->findOneById($old_entity_id);
            $old_entity->setArrangement(0);
            $em->persist($old_entity);
            $em->flush();
            $entity->setArrangement($new_kolejnosc);
            $em->persist($entity);
            $em->flush();
            $old_entity->setArrangement($old_kolejnosc);
            $em->persist($old_entity);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->add('success', 'Przeniesienie kategorii do góry zakończone sukcesem.');

        return new Response('OK');
    }


    public function moveEmployeesDownAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsEmployeesBundle:Employees')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }
        $category_id = $entity->getCategoryId();
        $max = $this->getMaxEmployeesKolejnosc($category_id);

        $old_kolejnosc = $entity->getArrangement();
        $new_kolejnosc = $old_kolejnosc + 1;
        if ($new_kolejnosc < $max) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsEmployeesBundle:Employees', 'c')
                ->where('c.arrangement = :arrangement')
                ->andWhere('c.category_id = :category_id')
                ->setParameter('arrangement', $new_kolejnosc)
                ->setParameter('category_id', $category_id)
                ->getQuery();

            $old_entity_id = $query->getSingleScalarResult();
            $old_entity = $em->getRepository('LsEmployeesBundle:Employees')->findOneById($old_entity_id);
            $old_entity->setArrangement(0);
            $em->persist($old_entity);
            $em->flush();
            $entity->setArrangement($new_kolejnosc);
            $em->persist($entity);
            $em->flush();
            $old_entity->setArrangement($old_kolejnosc);
            $em->persist($old_entity);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->add('success', 'Przeniesienie pracownika do dołu zakończone sukcesem.');

        return new Response('OK');
    }

    public function moveEmployeesUpAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsEmployeesBundle:Employees')->find($id);
        $category_id = $entity->getCategoryId();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        $old_kolejnosc = $entity->getArrangement();
        $new_kolejnosc = $old_kolejnosc - 1;
        if ($new_kolejnosc > 0) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsEmployeesBundle:Employees', 'c')
                ->where('c.arrangement = :arrangement')
                ->andWhere('c.category_id = :category_id')
                ->setParameter('arrangement', $new_kolejnosc)
                ->setParameter('category_id', $category_id)
                ->getQuery();

            $old_entity_id = $query->getSingleScalarResult();
            $old_entity = $em->getRepository('LsEmployeesBundle:Employees')->findOneById($old_entity_id);
            $old_entity->setArrangement(0);
            $em->persist($old_entity);
            $em->flush();
            $entity->setArrangement($new_kolejnosc);
            $em->persist($entity);
            $em->flush();
            $old_entity->setArrangement($old_kolejnosc);
            $em->persist($old_entity);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->add('success', 'Przeniesienie pracownika do góry zakończone sukcesem.');

        return new Response('OK');
    }


    public function staticAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('LsEmployeesBundle:StaticContent', 'e')
            ->where('e.bundle = :bundle')
            ->setParameter('bundle', 'employees')
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.description',
                'defaultSortDirection' => 'asc',
            )
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_employees'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Zespół', $this->get('router')->generate('ls_admin_employees'));
        $breadcrumbs->addItem('Zarządzaj treściami stałymi', $this->get('router')->generate('ls_admin_employees_static'));

        return $this->render('LsEmployeesBundle:Admin:static_list.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
        ));
    }

    public function staticEditAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsEmployeesBundle:StaticContent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Setting entity.');
        }

        $form = $this->createForm(StaticType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_employees_static_edit', array('id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja treści zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_employees_static_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_employees_static'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Zespół', $this->get('router')->generate('ls_admin_employees'));
        $breadcrumbs->addItem('Zarządzaj treściami stałymi', $this->get('router')->generate('ls_admin_employees_static'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_employees_static_edit', array('id' => $entity->getId())));

        return $this->render('LsEmployeesBundle:Admin:static_edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView()
        ));
    }

    public function staticDeleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsEmployeesBundle:StaticContent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Setting entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie treści zakończone sukcesem.');

        return new Response('OK');
    }

    public function staticNewAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new StaticContent();

        $form = $this->createForm(StaticType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_employees_static_new'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie triści zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_employees_static_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_employees_static'));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_employees_static_new'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Zespół', $this->get('router')->generate('ls_admin_employees'));
        $breadcrumbs->addItem('Zarządzaj treściami stałymi', $this->get('router')->generate('ls_admin_employees_static'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_employees_static_new'));

        return $this->render('LsEmployeesBundle:Admin:static_new.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
