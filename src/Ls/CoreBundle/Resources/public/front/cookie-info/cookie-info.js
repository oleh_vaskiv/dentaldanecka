var MG_jQuery;
function load_js_file(src, callback) {
    var s, r, t, h;
    r = false;
    s = document.createElement('script');
    s.type = 'text/javascript';
    s.src = src;
    s.onload = s.onreadystatechange = function () {
        if (!r && (!this.readyState || this.readyState === 'complete'))
        {
            r = true;
            callback();
        }
    };
    t = document.getElementsByTagName('script')[0];
    h = document.getElementsByTagName('head')[0];

    h.appendChild(s);
}

function load_css_file(filename) {
    var cssNode = document.createElement('link');
    cssNode.setAttribute('rel', 'stylesheet');
    cssNode.setAttribute('type', 'text/css');
    cssNode.setAttribute('media', 'screen');
    cssNode.setAttribute('href', filename);
    document.getElementsByTagName("head")[0].appendChild(cssNode);
}

function load_array_js_files(array, index, callback) {
    if (array[index]) {
        load_js_file(array[index], function () {
            load_array_js_files(array, index + 1, callback);
        });
    } else {
        callback();
    }
}

function load_array_css_files(array, callback) {
    var index = 0;
    while (array[index]) {
        load_css_file(array[index]);
        ++index;
    }
    callback();
}
var MG_loaded = false;
load_array_css_files([
    'https://www.dentaldanecka.pl/bundles/lscore/front/cookie-info/cookie-info.min.css',
], function () {
    load_js_file("https://www.dentaldanecka.pl/bundles/lscore/front/cookie-info/jquery-1.10.2.min.js", function () {
        MG_jQuery = jQuery.noConflict(true);
        load_array_js_files([
            "https://www.dentaldanecka.pl/bundles/lscore/front/cookie-info/jquery.cookie.min.js",
        ], 0, function () {
            MG_loaded = true;
            loadInfo();
        });
    });
});

function loadInfo() {
    var cookiesInfoHTML = '<div id="cookies-info"><div class="cookies-container">'+cookieInfoText+'<div class="accept" onclick="acceptCookies();">Akceptuję</div></div></div>';

    if ($.cookie('cookie') !== "true") {
        $('body').append(cookiesInfoHTML);
    }
}

function acceptCookies() {
    $.cookie('cookie', 'true', {expires: 365, path: '/'});
    $('#cookies-info').remove();
}