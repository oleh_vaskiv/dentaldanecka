<?php

namespace Ls\OfferBundle\Controller;

use Doctrine\DBAL\Types\Type;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Offer controller.
 *
 */

class FrontController extends Controller {
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();


        $static_content = array();
        $static_query = $em->createQueryBuilder()
            ->select('s')
            ->from('LsOfferBundle:StaticContent', 's')
            ->where('s.bundle = :bundle')
            ->setParameter('bundle', 'offer')
            ->getQuery()
            ->getResult();

        foreach ($static_query as $static) {
            $static_content[$static->getLabel()] = $static->getValue();
        }
        if ($static_content == null) $static_content = "";

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsOfferBundle:Offer', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->andWhere('a.parent_id IS NULL')
            ->orderBy('a.arrangement', 'ASC')
            ->setParameter('today', $today, Type::DATETIME)
            ->getQuery()
            ->getResult();

        return $this->render('LsOfferBundle:Front:index.html.twig', array(
            'entities' => $entities,
            'static_content' => $static_content,
        ));
    }

    public function getStaticContent($name, $default) {

    }

    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $today = new \DateTime();
        $qb = $em->createQueryBuilder();
        $offer = $qb->select('a')
            ->from('LsOfferBundle:Offer', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->andWhere('a.slug = :slug')
            ->setParameter('today', $today, Type::DATETIME)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();


        $parent_id = $offer->getId();
        $entities = $em->createQueryBuilder()
            ->select('e')
            ->from('LsOfferBundle:Offer', 'e')
            ->where($qb->expr()->isNotNull('e.published_at'))
            ->andWhere('e.published_at <= :today')
            ->andWhere('e.parent_id = :parent_id')
            ->orderBy('e.arrangement', 'ASC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setParameter('parent_id', $parent_id)
            ->getQuery()
            ->getResult();

        $categories = $em->createQueryBuilder()
            ->select('c')
            ->from('LsOfferBundle:Offer', 'c')
            ->where('c.slug <> :slug')
            ->andWhere('c.published_at <= :today')
            ->andWhere('c.parent_id IS NULL')
            ->orderBy('c.arrangement', 'ASC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getResult();
        
        if (!is_null($offer->getParentId())) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        } 

        if (null === $offer) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }  

        return $this->render('LsOfferBundle:Front:show.html.twig', array(
            'offer' => $offer,
            'entities' => $entities,
            'categories' => $categories,
        ));
    }

    public function showSubcategoryAction($slug, $subcategory_slug) {
        $em = $this->getDoctrine()->getManager();

        $today = new \DateTime();
        $qb = $em->createQueryBuilder();
        $offer = $qb->select('a')
            ->from('LsOfferBundle:Offer', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->andWhere('a.slug = :subcategory_slug')
            ->setParameter('today', $today, Type::DATETIME)
            ->setParameter('subcategory_slug', $subcategory_slug)
            ->getQuery()
            ->getOneOrNullResult();

        $parent_id = $offer->getParentId();
        $getParent = $em->getRepository('Ls\OfferBundle\Entity\Offer')->find($parent_id);

        $entities = $em->createQueryBuilder()
            ->select('e')
            ->from('LsOfferBundle:Offer', 'e')
            ->where($qb->expr()->isNotNull('e.published_at'))
            ->andWhere('e.published_at <= :today')
            ->andWhere('e.parent_id = :parent_id')
            ->orderBy('e.arrangement', 'ASC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setParameter('parent_id', $parent_id)
            ->getQuery()
            ->getResult();

        $categories = $em->createQueryBuilder()
            ->select('c')
            ->from('LsOfferBundle:Offer', 'c')
            ->where('c.slug <> :slug')
            ->andWhere('c.published_at <= :today')
            ->andWhere('c.parent_id IS NULL')
            ->orderBy('c.arrangement', 'ASC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getResult();

        if (null === $offer) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }  

        return $this->render('LsOfferBundle:Front:show_subcategory.html.twig', array(
            'offer' => $offer,
            'entities' => $entities,
            'categories' => $categories,
            'parent_slug' => $getParent->getSlug(),
        ));
    }

    public function ajaxMoreAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $page = $request->request->get('page');
        $start = $page * $limit;
        $allow = 1;

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsOfferBundle:Offer', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setFirstResult($start)
            ->getQuery()
            ->getResult();

        if (count($entities) < $limit) {
            $allow = 0;
        }

        $response = array(
            'allow' => $allow,
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsFormBundle:Offer:list.html.twig', array(
                'entities' => $entities,
            ))->getContent())
        );

        return new JsonResponse($response);
    }
}
