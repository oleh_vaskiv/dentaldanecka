<?php

namespace Ls\OfferBundle\Controller;

use Ls\OfferBundle\Entity\Offer;
use Ls\OfferBundle\Entity\StaticContent;
use Ls\OfferBundle\Form\OfferType;
use Ls\OfferBundle\Form\StaticType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller {
    private $pager_limit_name = 'admin_offer_pager_limit';

    public function indexAction($parent_id = null, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }
        if ($parent_id == null) {
            $query = $em->createQueryBuilder()
                ->select('e')
                ->from('LsOfferBundle:Offer', 'e')
                ->leftJoin('e.gallery', 'g')
                ->where('e.parent_id IS NULL')
                ->getQuery();
        } else {
            $query = $em->createQueryBuilder()
                ->select('e')
                ->from('LsOfferBundle:Offer', 'e')
                ->leftJoin('e.gallery', 'g')
                ->where('e.parent_id = :parent_id')
                ->setParameter('parent_id', $parent_id)
                ->getQuery();            
        }

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.arrangement',
                'defaultSortDirection' => 'asc',
            )
        );
        
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_offer'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Oferta', $this->get('router')->generate('ls_admin_offer'));
        
        if ($parent_id != NULL) {
            $parent = $em->getRepository('Ls\OfferBundle\Entity\Offer')->find($parent_id);
            $breadcrumbs->addItem($parent->__toString(), $this->get('router')->generate('ls_admin_offer_child', array('parent_id' => $parent_id)));
        }

        return $this->render('LsOfferBundle:Admin:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
            'parent_id' =>$parent_id,
        ));
    }

    public function listAction($parent_id = null, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }
        if ($parent_id == null) {
            $query = $em->createQueryBuilder()
                ->select('e')
                ->from('LsOfferBundle:Offer', 'e')
                ->leftJoin('e.gallery', 'g')
                ->where('e.parent_id IS NULL')
                ->getQuery();
        } else {
            $query = $em->createQueryBuilder()
                ->select('e')
                ->from('LsOfferBundle:Offer', 'e')
                ->leftJoin('e.gallery', 'g')
                ->where('e.parent_id = :parent_id')
                ->setParameter('parent_id', $parent_id)
                ->getQuery();            
        }

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.arrangement',
                'defaultSortDirection' => 'asc',
            )
        );
        
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_offer'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Oferta', $this->get('router')->generate('ls_admin_offer'));
        
        if ($parent_id != NULL) {
            $parent = $em->getRepository('Ls\OfferBundle\Entity\Offer')->find($parent_id);
            $breadcrumbs->addItem($parent->__toString(), $this->get('router')->generate('ls_admin_offer_child', array('parent_id' => $parent_id)));
        }

        return $this->render('LsOfferBundle:Admin:list.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
            'parent_id' =>$parent_id,
        ));
    }


    public function newAction($parent_id = null, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new Offer();
        if ($parent_id == null) 
            $child = '';
        else
            $child = '_child';

        $form = $this->createForm(OfferType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_offer_new'.$child.'', array('parent_id' => $parent_id)),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {

            $em->persist($entity);
            $em->flush();

            $sitemap = $this->get('ls_core.sitemap');
            $sitemap->generate();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_offer_edit'.$child.'', array('parent_id' => $parent_id, 'id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_offer'.$child.'', array('parent_id' => $parent_id)));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_offer_new'.$child.'', array('parent_id' => $parent_id)));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Oferta', $this->get('router')->generate('ls_admin_offer'));
        if ($parent_id != NULL) {
            $parent = $em->getRepository('Ls\OfferBundle\Entity\Offer')->find($parent_id);
            $breadcrumbs->addItem($parent->__toString(), $this->get('router')->generate('ls_admin_offer_child', array('parent_id' => $parent_id)));
        }
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_offer_new'));

        return $this->render('LsOfferBundle:Admin:new.html.twig', array(
            'form' => $form->createView(),
            'parent_id' => $parent_id
        ));
    }

    public function editAction($parent_id = null, $id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsOfferBundle:Offer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find offer entity.');
        }

        if ($parent_id == null) 
            $child = '';
        else
            $child = '_child';

        $form = $this->createForm(OfferType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_offer_edit'.$child.'', array( 'parent_id' => $parent_id, 'id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $sitemap = $this->get('ls_core.sitemap');
            $sitemap->generate();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja pracownika zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_offer_edit'.$child.'', array('parent_id' => $parent_id, 'id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_offer'.$child.'',  array('parent_id' => $parent_id)));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Oferta', $this->get('router')->generate('ls_admin_offer'));
        if ($parent_id != NULL) {
            $parent = $em->getRepository('Ls\OfferBundle\Entity\Offer')->find($parent_id);
            $breadcrumbs->addItem($parent->__toString(), $this->get('router')->generate('ls_admin_offer_child', array('parent_id' => $parent_id)));
        }
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_offer_edit'.$child.'', array('parent_id' => $parent_id, 'id' => $entity->getId())));

        return $this->render('LsOfferBundle:Admin:edit.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsOfferBundle:Offer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        $em->remove($entity);
        $em->flush();

        $sitemap = $this->get('ls_core.sitemap');
        $sitemap->generate();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie pracownika zakończone sukcesem.');

        return new Response('OK');
    }

    public function batchAction($category_id, Request $request) {
        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Oferta', $this->get('router')->generate('ls_admin_offer'));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_offer_batch'));

            return $this->render('LsOfferBundle:Admin:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
                'category_id' => $category_id,
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_offer'));
        }
    }

    public function batchExecuteAction($category_id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsOfferBundle:Offer', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }

                    $sitemap = $this->get('ls_core.sitemap');
                    $sitemap->generate();

                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_offer_category_list', array('id' => $category_id )));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_offer_category_list', array('id' => $category_id )));
        }
    }

    private function getMaxKolejnosc() {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsOfferBundle:Offer', 'c')
            ->getQuery();

        $total = $query->getSingleScalarResult();
        return $total + 1;
    }

    public function moveOfferDownAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsOfferBundle:Offer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }
        $parent_id = $entity->getParentId();
        $max = $this->getMaxKolejnosc($parent_id);

        $old_kolejnosc = $entity->getArrangement();
        $new_kolejnosc = $old_kolejnosc + 1;
        if ($new_kolejnosc < $max) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsOfferBundle:Offer', 'c')
                ->where('c.arrangement = :arrangement')
                ->andWhere('c.parent_id = :parent_id')
                ->setParameter('arrangement', $new_kolejnosc)
                ->setParameter('parent_id', $parent_id)
                ->getQuery();

            $old_entity_id = $query->getSingleScalarResult();
            $old_entity = $em->getRepository('LsOfferBundle:Offer')->findOneById($old_entity_id);
            $old_entity->setArrangement(0);
            $em->persist($old_entity);
            $em->flush();
            $entity->setArrangement($new_kolejnosc);
            $em->persist($entity);
            $em->flush();
            $old_entity->setArrangement($old_kolejnosc);
            $em->persist($old_entity);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->add('success', 'Przeniesienie pracownika do dołu zakończone sukcesem.');

        return new Response('OK');
    }

    public function moveOfferUpAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsOfferBundle:Offer')->find($id);
        $parent_id = $entity->getParentId();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        $old_kolejnosc = $entity->getArrangement();
        $new_kolejnosc = $old_kolejnosc - 1;
        if ($new_kolejnosc > 0) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsOfferBundle:Offer', 'c')
                ->where('c.arrangement = :arrangement')
                ->andWhere('c.parent_id = :parent_id')
                ->setParameter('arrangement', $new_kolejnosc)
                ->setParameter('parent_id', $parent_id)
                ->getQuery();

            $old_entity_id = $query->getSingleScalarResult();
            $old_entity = $em->getRepository('LsOfferBundle:Offer')->findOneById($old_entity_id);
            $old_entity->setArrangement(0);
            $em->persist($old_entity);
            $em->flush();
            $entity->setArrangement($new_kolejnosc);
            $em->persist($entity);
            $em->flush();
            $old_entity->setArrangement($old_kolejnosc);
            $em->persist($old_entity);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->add('success', 'Przeniesienie pracownika do góry zakończone sukcesem.');

        return new Response('OK');
    }

    public function staticAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('LsOfferBundle:StaticContent', 'e')
            ->where('e.bundle = :bundle')
            ->setParameter('bundle', 'offer')
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.description',
                'defaultSortDirection' => 'asc',
            )
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_offer'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Oferta', $this->get('router')->generate('ls_admin_offer'));
        $breadcrumbs->addItem('Zarządzaj treściami stałymi', $this->get('router')->generate('ls_admin_offer_static'));

        return $this->render('LsOfferBundle:Admin:static_list.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
        ));
    }

    public function staticEditAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsOfferBundle:StaticContent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Setting entity.');
        }

        $form = $this->createForm(StaticType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_offer_static_edit', array('id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja treści zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_offer_static_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_offer_static'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Oferta', $this->get('router')->generate('ls_admin_offer'));
        $breadcrumbs->addItem('Zarządzaj treściami stałymi', $this->get('router')->generate('ls_admin_offer_static'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_offer_static_edit', array('id' => $entity->getId())));

        return $this->render('LsOfferBundle:Admin:static_edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView()
        ));
    }

    public function staticDeleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsOfferBundle:StaticContent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Setting entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie treści zakończone sukcesem.');

        return new Response('OK');
    }

    public function staticNewAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new StaticContent();

        $form = $this->createForm(StaticType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_offer_static_new'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie triści zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_offer_static_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_offer_static'));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_offer_static_new'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Zespół', $this->get('router')->generate('ls_admin_offer'));
        $breadcrumbs->addItem('Zarządzaj treściami stałymi', $this->get('router')->generate('ls_admin_offer_static'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_offer_static_new'));

        return $this->render('LsOfferBundle:Admin:static_new.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
