$(function () {
    fckconfig_common = {
        skin: 'BootstrapCK-Skin',
        contentsCss: ['/bundles/lscore/front/css/editor.css'],
        bodyClass: 'editor',
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}},
            {name: 'Nagłówek 1', element: 'div', attributes: {'class': 'title-1'}},
            {name: 'Cytat', element: 'div', attributes: {'class': 'cytat'}},
            {name: 'Wypunktowanie', element: 'div', attributes: {'class': 'blist'}},
            {name: 'Nagłówek (St Gł)', element: 'div', attributes: {'class': 'title'}},
            {name: 'Styl 2 (St Gł)', element: 'div', attributes: {'class': 'text-10'}},
            {name: 'Zielony', element: 'span', attributes: {'class': 'green'}},
            {name: 'Fioletowy', element: 'span', attributes: {'class': 'purple'}},
            {name: 'Cytat 2', element: 'div', attributes: {'class': 'team-note'}},
            {name: 'Nagłówek 3', element: 'div', attributes: {'class': 'team-title'}},
            {name: 'Plik', element: 'a', attributes: {'class': 'added-file'}}
        ],
        filebrowserBrowseUrl: kcfinderBrowseUrl + '?type=files',
        filebrowserImageBrowseUrl: kcfinderBrowseUrl + '?type=images',
        filebrowserFlashBrowseUrl: kcfinderBrowseUrl + '?type=flash',
        filebrowserUploadUrl: '/bundles/lscore/common/kcfinder-3.12/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lscore/common/kcfinder-3.12/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lscore/common/kcfinder-3.12/upload.php?type=flash'
    };

    fckconfig = jQuery.extend(true, {
        height: '400px',
        width: 'auto'
    }, fckconfig_common);

    $('.wysiwyg').ckeditor(fckconfig);
});