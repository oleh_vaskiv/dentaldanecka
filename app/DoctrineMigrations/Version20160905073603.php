<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160905073603 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE advices RENAME INDEX idx_1dd399504e7af8f TO IDX_47DE8B044E7AF8F');
        $this->addSql('ALTER TABLE employees DROP FOREIGN KEY FK_1GG399504E7AF8C');
        $this->addSql('ALTER TABLE employees DROP FOREIGN KEY FK_1GG399504E7AF8D');
        $this->addSql('DROP INDEX FK_1GG399504E7AF8D ON employees');
        $this->addSql('DROP INDEX FK_1GG399504E7AF8C ON employees');
        $this->addSql('ALTER TABLE employees DROP gallery_id, CHANGE category_id category_id INT NOT NULL, CHANGE in_slider in_slider TINYINT(1) NOT NULL, CHANGE post post VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE offer DROP FOREIGN KEY FK_1GG399504E7AF8M');
        $this->addSql('DROP INDEX offer_ibfk_1 ON offer');
        $this->addSql('DROP INDEX parent_id ON offer');
        $this->addSql('ALTER TABLE offer CHANGE parent_id parent_id INT NOT NULL, CHANGE last last TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE offer RENAME INDEX fk_1gg399504e7af8g TO IDX_29D6873E4E7AF8F');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Advices RENAME INDEX idx_47de8b044e7af8f TO IDX_1DD399504E7AF8F');
        $this->addSql('ALTER TABLE Employees ADD gallery_id INT DEFAULT NULL, CHANGE category_id category_id INT DEFAULT NULL, CHANGE in_slider in_slider TINYINT(1) DEFAULT NULL, CHANGE post post VARCHAR(255) DEFAULT \'Nie ustalono\' COLLATE utf8mb4_general_ci');
        $this->addSql('ALTER TABLE Employees ADD CONSTRAINT FK_1GG399504E7AF8C FOREIGN KEY (category_id) REFERENCES employees_categories (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE Employees ADD CONSTRAINT FK_1GG399504E7AF8D FOREIGN KEY (gallery_id) REFERENCES gallery (id)');
        $this->addSql('CREATE INDEX FK_1GG399504E7AF8D ON Employees (gallery_id)');
        $this->addSql('CREATE INDEX FK_1GG399504E7AF8C ON Employees (category_id)');
        $this->addSql('ALTER TABLE offer CHANGE parent_id parent_id INT DEFAULT NULL, CHANGE last last TINYINT(1) DEFAULT \'0\'');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_1GG399504E7AF8M FOREIGN KEY (parent_id) REFERENCES offer (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('CREATE INDEX offer_ibfk_1 ON offer (parent_id)');
        $this->addSql('CREATE INDEX parent_id ON offer (parent_id)');
        $this->addSql('ALTER TABLE offer RENAME INDEX idx_29d6873e4e7af8f TO FK_1GG399504E7AF8G');
    }
}
