<?php

namespace Ls\EmployeesBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Ls\CoreBundle\Utils\Tools;
use Ls\GalleryBundle\Entity\HasGallery;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * employees_categories
 * @ORM\Table(name="employees_categories")
 * @ORM\Entity
 */
class EmployeesCategories
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=11)
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $category_name;

    /**
     * Constructor
     */
    public function __construct()
    {   
        //----------
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement()
    {
        return $this->arrangement;
    }

    /**
     * Set arrangement
     *
     * @return integer
     */
    public function setArrangement($arrangement)
    {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Set category_name
     *
     * @param string $name
     * @return Employees
     */
    public function setCategory_Name($category_name)
    {
        $this->category_name = $category_name;

        return $this;
    }

    /**
     * Get category_name
     *
     * @return string
     */
    public function getCategory_Name()
    {
        return $this->category_name;
    }

    public function setCategoryName($category_name)
    {
        $this->category_name = $category_name;

        return $this;
    }

    /**
     * Get category_name
     *
     * @return string
     */
    public function getCategoryName()
    {
        return $this->category_name;
    }

    public function __toString()
    {
        if (is_null($this->getCategory_Name())) {
            return 'NULL';
        }

        return $this->getCategory_Name();
    }
}