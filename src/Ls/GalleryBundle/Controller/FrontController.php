<?php

namespace Ls\GalleryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Gallery controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all Gallery entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsGalleryBundle:Gallery', 'a')
            ->getQuery()
            ->getResult();

        return $this->render('LsGalleryBundle:Front:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Gallery entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.photos', 'p')
            ->where('g.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        return $this->render('LsGalleryBundle:Front:show.html.twig', array(
            'entity' => $entity,
        ));
    }
}
