<?php

namespace Ls\ContactBundle\Form;

use Ls\CoreBundle\Form\GoogleRecaptchaType;
use Ls\CoreBundle\Validator\Constraints\GoogleRecaptcha;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', null, array(
                'label' => 'Twoje imię:',
            )
        );
        $builder->add('email', null, array(
                'label' => 'Twój adres e-mail:',
            )
        );
        $builder->add('content', TextareaType::class, array(
                'required' => false,
                'label' => 'Treść zapytania:',
            )
        );

        $builder->add('phone', null, array(
                'label' => 'Twój numer telefonu:',
                'attr' => array('size' => '20'),
            )
        );
    
        $builder->add('accept', CheckboxType::class, array(
            'required' => false,
            'mapped' => false,
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Zaznacz pole'
                )),
            )
        ));
        
        $builder->add('submit', SubmitType::class, array(
                'label' => 'Wyślij wiadomość'
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\ContactBundle\Entity\Contact',
            ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_contact';
    }
}
