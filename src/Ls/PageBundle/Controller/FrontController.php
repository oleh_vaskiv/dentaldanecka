<?php

namespace Ls\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\DBAL\Types\Type;
/**
 * Page controller.
 *
 */
class FrontController extends Controller {

    /**
     * Finds and displays a Page entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsPageBundle:Page', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();


        $count = $em->createQueryBuilder()
             ->select('COUNT(u)')
             ->from('LsEmployeesBundle:Employees', 'u')
             ->getQuery()
             ->getSingleScalarResult();

        $employeer = $em->createQueryBuilder()
            ->select('e')
            ->from('LsEmployeesBundle:Employees', 'e')
            ->setFirstResult(rand(0, $count - 1))
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
       

        $today = new \DateTime();
        $last_advice = $em->createQueryBuilder()
            ->select('a')
            ->from('LsAdvicesBundle:Advices', 'a')
            ->where($em->createQueryBuilder()->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        return $this->render('LsPageBundle:Front:show.html.twig', array(
            'entity' => $entity,
            'employeer' => $employeer,
            'advice' => $last_advice,
        ));
    }
    
    /**
     * Finds and displays gala beauty stars page.
     *
     */
    public function showGalaAction() {
        
        return $this->render('LsPageBundle:Front:showGala.html.twig', array(
        ));
    }

}
