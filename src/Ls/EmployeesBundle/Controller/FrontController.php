<?php

namespace Ls\EmployeesBundle\Controller;

use Doctrine\DBAL\Types\Type;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Employees controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all Employees entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $allow = 1;

        $today = new \DateTime();

        $static_content = array('');
        $static_query = $em->createQueryBuilder()
            ->select('s')
            ->from('LsOfferBundle:StaticContent', 's')
            ->where('s.bundle = :bundle')
            ->setParameter('bundle', 'employees')
            ->getQuery()
            ->getResult();

        foreach ($static_query as $static) {
            $static_content[$static->getLabel()] = $static->getValue();
        }

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsEmployeesBundle:Employees', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.arrangement', 'ASC')
            ->setParameter('today', $today, Type::DATETIME)
            ->getQuery()
            ->getResult();

        $categories = $qb->select('c')
            ->from('LsEmployeesBundle:EmployeesCategories', 'c')
            ->orderBy('c.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('LsEmployeesBundle:Front:index.html.twig', array(
            'entities' => $entities,
            'categories' => $categories,
            'static_content' => $static_content,
        ));
    }

    /**
     * Finds and displays a Employees entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $today = new \DateTime();
        $qb = $em->createQueryBuilder();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsEmployeesBundle:Employees', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        $entities = $qb->select('a')
            ->from('LsEmployeesBundle:Employees', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->andWhere('a.slug <> :slug')
            ->andWhere('a.category_id = :category_id')
            ->orderBy('a.arrangement', 'ASC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setParameter('slug', $slug)
            ->setParameter('category_id', $entity->getCategoryId())
            ->getQuery()
            ->getResult();

        $today = new \DateTime();
        $advice = $em->createQueryBuilder()
            ->select('a')
            ->from('LsAdvicesBundle:Advices', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();



        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Employees entity.');
        }

        return $this->render('LsEmployeesBundle:Front:show.html.twig', array(
            'entity' => $entity,
            'entities' => $entities,
            'advice' => $advice,
        ));
    }

    public function ajaxMoreAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $page = $request->request->get('page');
        $start = $page * $limit;
        $allow = 1;

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsEmployeesBundle:Employees', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setFirstResult($start)
            ->getQuery()
            ->getResult();

        if (count($entities) < $limit) {
            $allow = 0;
        }

        $response = array(
            'allow' => $allow,
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsFormBundle:Employees:list.html.twig', array(
                'entities' => $entities,
            ))->getContent())
        );

        return new JsonResponse($response);
    }
}
