<?php

namespace Ls\CoreBundle\Controller;

use Ls\CoreBundle\Entity\Advices;
use Doctrine\DBAL\Types\Type;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminDashboard;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Ls\SliderBundle\Entity\Slider;

class DefaultController extends Controller {
    protected $containerBuilder;

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $limit = 2;
        $allow = 1;

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();

        $entities = $qb->select('a')
            ->from('LsAdvicesBundle:Advices', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        $employees = $em->createQueryBuilder()
            ->select('s')
            ->from('LsEmployeesBundle:Employees', 's')
            ->where('s.in_slider = :slider')
            ->setParameter('slider', '1')
            ->orderBy('s.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        $offer = $em->createQueryBuilder()
            ->select('f')
            ->from('LsOfferBundle:Offer', 'f')
            ->where('f.published_at <= :today')
            ->andWhere('f.parent_id IS NULL')
            ->orderBy('f.arrangement', 'ASC')
            ->setParameter('today', $today, Type::DATETIME)
            ->getQuery()
            ->getResult();

        foreach ($entities as $entity) {
            if ($entity->getPhotoWebPath()) {
                $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 170, '...'));
            } else {
                $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 255, '...'));
            }
        }

        return $this->render('LsCoreBundle:Default:index.html.twig', array(
                'entities' => $entities,
                'employees' => $employees,
                'offer' => $offer,
                'allow' => $allow

            ));
    }

    public function adminAction() {
        $blocks = $this->container->getParameter('ls_core.admin.dashboard');
        $dashboard = new AdminDashboard();

        foreach($blocks as $block) {
            $parent = new AdminBlock($block['label']);
            $dashboard->addBlock($parent);
            foreach($block['items'] as $item) {
                $service = $this->container->get($item);
                $service->addToDashboard($parent);
            }
        }

        return $this->render('LsCoreBundle:Default:admin.html.twig', array(
            'dashboard' => $dashboard,
        ));
    }

    public function KCFinderAction(Request $request)
    {
        $upload_dir = $this->get('kernel')->getRootDir() . '/../web/upload/pliki';

        $_SESSION['KCFINDER']['disabled'] = false;
        $_SESSION['KCFINDER']['uploadDir'] = $upload_dir;

        $getParameters = $request->query->all();

        
        return new RedirectResponse(
            $request->getBasePath() . 
            '/bundles/lscore/common/kcfinder-3.20/browse.php?' . 
            http_build_query($getParameters));
    }
}

